import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import {LoginComponent} from './components/login/login.component'
import { SimpleNotificationsModule } from 'angular2-notifications';
import { ComponentsStateButtonModule } from 'src/app/components/state-button/components.state-button.module';

@NgModule({
    imports: [
        CommonModule,
        AdminRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        SimpleNotificationsModule.forRoot(),
        ComponentsStateButtonModule
    ],
    declarations: [LoginComponent],
    exports: []
})
export class AdminModule {
}