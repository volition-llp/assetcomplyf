export class loginResponse{
    status : string;
    message: string;
    token: string;
    username: string;
}