import { Component, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import {AuthenticationService} from '../../services/authentication-service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm') loginForm: NgForm;
  buttonDisabled = false;
  buttonState = '';
  constructor(
    private notifications: NotificationsService,
    private authService: AuthenticationService,
    private renderer: Renderer2
    ) { }

  ngOnInit(): void {
    this.renderer.addClass(document.body, 'background');
    this.renderer.addClass(document.body, 'no-footer');
  }
 
  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'background');
    this.renderer.removeClass(document.body, 'no-footer');
  }
  
  onSubmit() {
    if (!this.loginForm.valid || this.buttonDisabled) {
      return;
    }
    console.log('On login click');
    this.buttonState = 'show-spinner';
    this.buttonDisabled = true;

    this.authService.adminlogin(this.loginForm.value.username, this.loginForm.value.password).subscribe(
      data=>{
        console.log(data);
        if (data.status == '900') {
          this.notifications.create(
            'Welcome',
            data.message,
            NotificationType.Success,
            {
              theClass: 'outline primary',
              timeOut: 6000,
              showProgressBar: false,
            }
          );
          this.buttonDisabled = false;
          this.buttonState = '';
          localStorage.setItem('admin_token', data.token)
          localStorage.setItem('admin_name', data.username)
          // localStorage.setItem('usr_cmp', data.company)
          // this.router.navigateByUrl('/app/dashboards')
        } else {
          this.notifications.create(
            'Error',
            'Login Failed:'+ data.message,
            NotificationType.Error,
            {
              theClass: 'outline primary',
              timeOut: 6000,
              showProgressBar: false,
            }
          );
          this.buttonDisabled = false;
          this.buttonState = '';
        }
      },
      err=>{
        this.notifications.create(
          'Error',
          'Login Failed, due to some error.',
          NotificationType.Error,
          {
            theClass: 'outline danger',
            timeOut: 6000,
            showProgressBar: false,
          }
        );
        this.buttonDisabled = false;
        this.buttonState = '';
      }
    );
  }

  onLoginClick(){
   
  }
}
