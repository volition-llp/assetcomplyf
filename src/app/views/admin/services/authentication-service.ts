import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { loginResponse } from '../model/response/loginresponse';
import { loginRequest } from '../model/request/loginrequest';
import { environment } from '../../../../environments/environment'

@Injectable({
    providedIn:'root'
})

export class AuthenticationService{
    
    loginUrl = environment.BASE_URL+'admin/login';

    constructor(
        private readonly httpObj: HttpClient
    ){        
    }

    adminlogin(username:string,password:string):Observable<loginResponse>{
        var request:loginRequest = new loginRequest();
        request.username = username;
        request.password = password;
        
        return this.httpObj.post<loginResponse>(this.loginUrl,request);
    }
}