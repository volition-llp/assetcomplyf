import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; 
import { CustLoginComponent } from './components/customer-login/custLogin.component'


const routes: Routes = [ 
     {path:'', component: CustLoginComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
  })
export class CustomerRoutingModule { }
