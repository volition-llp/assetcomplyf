export class custLoginResponse{
    status : string;
    message: string;
    token: string;
    username: string;
    company: string;
}