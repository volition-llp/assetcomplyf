import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { custLoginResponse } from '../model/response/cust-login-response';
import { custLoginRequest } from '../model/request/cust-login-request';
import { environment } from '../../../../environments/environment'

@Injectable({
    providedIn:'root'
})

export class customerAuthenticationService{
    
    loginUrl = environment.BASE_URL+'user/login';

    constructor(
        private readonly httpObj: HttpClient
    ){        
    }

    customerlogin(username:string,password:string, company: string):Observable<custLoginResponse>{
        var request:custLoginRequest = new custLoginRequest();
        request.username = username;
        request.password = password;
        request.company = company;
        
        return this.httpObj.post<custLoginResponse>(this.loginUrl, request);
    }
}